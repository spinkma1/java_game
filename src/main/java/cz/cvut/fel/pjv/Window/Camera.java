package cz.cvut.fel.pjv.Window;

import cz.cvut.fel.pjv.Controllers.Game;
import cz.cvut.fel.pjv.Utils.GameObject;

public class Camera {
    private float x, y;
    Game game;

    /**
     * Constructs a new Camera object.
     *
     * @param x    the initial x-coordinate of the camera
     * @param y    the initial y-coordinate of the camera
     * @param game the game instance
     */
    public Camera(int x, int y, Game game) {
        this.x = x;
        this.y = y;
        this.game = game;
    }

    /**
     * Updates the camera's position based on the player's position.
     *
     * @param player the player object
     */
    public void update(GameObject player) {
        float playerX = player.getX();
        float playerY = player.getY();

        x = -playerX + Game.WIDTH / 2;
        y = -playerY + Game.HEIGHT / 2;
    }

    /**
     * Returns the x-coordinate of the camera.
     *
     * @return the x-coordinate of the camera
     */
    public float getX() {
        return x;
    }

    /**
     * Sets the x-coordinate of the camera.
     *
     * @param x the new x-coordinate of the camera
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * Returns the y-coordinate of the camera.
     *
     * @return the y-coordinate of the camera
     */
    public float getY() {
        return y;
    }

    /**
     * Sets the y-coordinate of the camera.
     *
     * @param y the new y-coordinate of the camera
     */
    public void setY(float y) {
        this.y = y;
    }
}