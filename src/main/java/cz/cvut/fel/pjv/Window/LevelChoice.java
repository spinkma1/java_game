package cz.cvut.fel.pjv.Window;

import cz.cvut.fel.pjv.Controllers.Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class LevelChoice extends JFrame {
    private JComboBox<String> levelSelect;
    private JButton selectButton;
    private Game game;

    /**
     * Constructs a new LevelChoice object.
     *
     * @param game the game instance
     */
    public LevelChoice(Game game) {
        this.game = game;
        setTitle("Choose save");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());

        String directoryPath = "src/main/resources/saves";

        File directory = new File(directoryPath);
        String[] files = directory.list((dir, name) -> name.endsWith(".png"));

        // select with choices
        levelSelect = new JComboBox<>(files);
        add(levelSelect);

        // button to load level
        selectButton = new JButton("Select level");
        selectButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String selectedFileName = (String) levelSelect.getSelectedItem();
                if (selectedFileName != null) {
                    game.setLevel_name(selectedFileName);
                    setVisible(false);
                }
            }
        });
        add(selectButton);

        pack(); // automatic re-adjust to display's size
        setLocationRelativeTo(null); // locate a form in the center of the display
    }
}