package cz.cvut.fel.pjv.Window;
import cz.cvut.fel.pjv.Controllers.Game;

import javax.swing.*;
import java.awt.Dimension;
/**

 The Window class represents the main game window.
 It creates and configures the JFrame window to display the game.
 */
public class Window {
    /**
     * Constructs a new Window object.
     *
     * @param width the width of the game window
     * @param height the height of the game window
     * @param game the game instance
     * @param name the name of the game window
     */
    public Window(int width, int height, Game game, String name) {
        game.setPreferredSize(new Dimension(width,height));
        game.setMaximumSize(new Dimension(width,height));
        game.setMinimumSize(new Dimension(width,height));
        JFrame frame= new JFrame(name);
        frame.add(game);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        game.start();
    }
}
