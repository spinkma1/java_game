package cz.cvut.fel.pjv.Utils;

import com.google.gson.annotations.Expose;
import cz.cvut.fel.pjv.Objects.ObjectType;

import java.awt.*;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * The GameObject class represents a generic game object.
 * It serves as a base class for specific game objects in the game.
 */
public abstract class GameObject implements Serializable {

    @Expose
    protected float x, y;
    protected float Xcoor = 0, Ycoor = 0;
    @Expose
    protected ObjectType type;

    protected boolean JUMPING = false;

    @Expose
    protected boolean isInGame = true;

    protected boolean FALLING = true;

    /**
     * Constructs a new GameObject with the specified coordinates and type.
     *
     * @param x    the x-coordinate of the object's position
     * @param y    the y-coordinate of the object's position
     * @param type the type of the object
     */
    public GameObject(float x, float y, ObjectType type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }

    /**
     * Updates the object's state and behavior.
     *
     * @param object the list of all game objects
     */
    public abstract void tick(LinkedList<GameObject> object);

    /**
     * Performs the object's attack action.
     *
     * @param flag true if the object is attacking, false otherwise
     */
    public abstract void attack(Boolean flag);

    /**
     * Renders the object on the screen.
     *
     * @param g the graphics context
     */
    public abstract void render(Graphics g);

    /**
     * Returns the bounding rectangle for collision detection.
     *
     * @return the bounding rectangle
     */
    public abstract Rectangle getBounds();

    /**
     * Returns the amount of inventory items held by the object.
     *
     * @return the inventory amount
     */
    public abstract int getInventoryAmount();

    /**
     * Returns the inventory of the object.
     *
     * @return the object's inventory
     */
    public abstract Inventory getInventory();

    /**
     * Returns the x-coordinate of the object's position.
     *
     * @return the x-coordinate
     */
    public float getX() {
        return x;
    }

    /**
     * Returns the y-coordinate of the object's position.
     *
     * @return the y-coordinate
     */
    public float getY() {
        return y;
    }

    /**
     * Sets the x-coordinate of the object's position.
     *
     * @param x the x-coordinate
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * Sets the y-coordinate of the object's position.
     *
     * @param y the y-coordinate
     */
    public void setY(float y) {
        this.y = y;
    }

    /**
     * Sets the X coordinate of the object's position.
     *
     * @param Xcoor the X coordinate
     */
    public void setXcoor(float Xcoor) {
        this.Xcoor = Xcoor;
    }

    /**
     * Sets the Y coordinate of the object's position.
     *
     * @param Ycoor the Y coordinate
     */
    public void setYcoor(float Ycoor) {
        this.Ycoor = Ycoor;
    }

    /**
     * Returns the type of the object.
     *
     * @return the object type
     */
    public ObjectType getType() {
        return type;
    }

    /**
     * Checks if the object is in the jumping state.
     *
     * @return true if the object is jumping, false otherwise
     */
    public boolean isJUMPING() {
        return JUMPING;
    }

    /**
     * Sets the jumping state of the object.
     *
     * @param JUMPING true if the object is jumping, false otherwise
     */
    public void setJUMPING(boolean JUMPING) {
        this.JUMPING = JUMPING;
    }

    /**
     * Checks if the object is in the game.
     *
     * @return true if the object is in the game, false otherwise
     */
    public boolean isInGame() {
        return isInGame;
    }

    /**
     * Sets the in-game state of the object.
     *
     * @param inGame true if the object is in the game, false otherwise
     */
    public void setInGame(boolean inGame) {
        isInGame = inGame;
    }
    /**
     * Checks if the loggers for the objects are on.
     *
     * @return true if loggers are on.
     */

}

