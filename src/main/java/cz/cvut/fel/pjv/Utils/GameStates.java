package cz.cvut.fel.pjv.Utils;

import cz.cvut.fel.pjv.Controllers.Game;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * The GameStates class represents different states of the game.
 * It handles rendering of various game screens such as the main menu, pause menu, death scene, and win screen.
 */
public class GameStates {

    public Rectangle playButton = new Rectangle(Game.WIDTH / 2 - 100, 150, 200, 50);
    public Rectangle loadSaveButton = new Rectangle(Game.WIDTH / 2 - 100, 250, 200, 50);
    public Rectangle saveButton = new Rectangle(Game.WIDTH / 2 - 100, 150, 200, 50);
    public Rectangle exitButton = new Rectangle(Game.WIDTH / 2 - 100, 250, 200, 50);
    public Rectangle quitButton = new Rectangle(Game.WIDTH / 2 - 100, 350, 200, 50);
    public Rectangle loggerButton = new Rectangle(Game.WIDTH / 2 - 100, 450, 200, 50);

    private BufferedImage backgroundImage;
    private final Game game;

    /**
     * Constructs a new GameStates object with the specified game instance.
     *
     * @param game the game instance
     */
    public GameStates(Game game) {
        this.game = game;
    }

    /**
     * Renders the main menu screen.
     *
     * @param g the graphics context
     */
    public void render(Graphics g) {
        try {
            backgroundImage = ImageIO.read(new File("src/main/resources/background.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Font customFont = loadCustomFont("src/main/resources/Mediaval_font.ttf", false);
        Color clr = new Color(72, 174, 198);
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(backgroundImage, 0, 0, 800, 600, null);
        Font customFontMain = loadCustomFont("src/main/resources/Mediaval_font.ttf", true);
        g.setFont(customFontMain);
        g.setColor(clr);
        g.drawString("Spider's Fury", Game.WIDTH / 2 - 150, 100);

        g.setFont(customFont);
        g.drawString("Start", playButton.x + 15, playButton.y + 30);
        g2d.draw(playButton);
        g.drawString("Load save", loadSaveButton.x + 15, loadSaveButton.y + 30);
        g2d.draw(loadSaveButton);
        g.drawString("Quit", quitButton.x + 15, quitButton.y + 30);
        g2d.draw(quitButton);
        g.drawString("Turn on logging", loggerButton.x + 15, loggerButton.y + 30);
        g2d.draw(loggerButton);
    }

    /**
     * Renders the pause menu screen.
     *
     * @param g the graphics context
     */
    public void renderPause(Graphics g) {
        try {
            backgroundImage = ImageIO.read(new File("src/main/resources/background.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Font customFont = loadCustomFont("src/main/resources/Mediaval_font.ttf", false);
        Color clr = new Color(72, 174, 198);
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(backgroundImage, 0, 0, 800, 600, null);
        Font customFontMain = loadCustomFont("src/main/resources/Mediaval_font.ttf", true);
        g.setFont(customFontMain);
        g.setColor(clr);
        g.drawString("Paused", Game.WIDTH / 2 - 85, 100);
        g.setFont(customFont);
        g.drawString("Save", saveButton.x + 15, saveButton.y + 30);
        g2d.draw(saveButton);
        g.drawString("Exit", exitButton.x + 15, exitButton.y + 30);
        g2d.draw(exitButton);
    }

    /**
     * Renders the death scene screen.
     *
     * @param g the graphics context
     * @param x the x-coordinate of the scene
     * @param y the y-coordinate of the scene
     */
    public void renderDeathScene(Graphics g, float x, float y) {
        Game.setGameover(true);
        try {
            backgroundImage = ImageIO.read(new File("src/main/resources/background.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Font customFont = loadCustomFont("src/main/resources/Mediaval_font.ttf", false);
        Color clr = new Color(72, 174, 198);
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(backgroundImage, (int) x - 400, (int) y - 300, 800, 600, null);
        Font customFontMain = loadCustomFont("src/main/resources/Mediaval_font.ttf", true);
        g.setFont(customFontMain);
        g.setColor(clr);
        Rectangle deathButton = new Rectangle((int) x - 100, (int) y - 150, 200, 50);
        g.drawString("Game over", (int) x - 100, (int) y - 200);
        g.setFont(customFont);
        g.drawString("Exit", deathButton.x + 15, deathButton.y + 30);
        g2d.draw(deathButton);
    }

    /**
     * Renders the win screen.
     *
     * @param g the graphics context
     * @param x the x-coordinate of the screen
     * @param y the y-coordinate of the screen
     */
    public void renderWinScreen(Graphics g, float x, float y) {
        Game.setGameover(true);
        try {
            backgroundImage = ImageIO.read(new File("src/main/resources/background.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Font customFont = loadCustomFont("src/main/resources/Mediaval_font.ttf", false);
        Color clr = new Color(72, 174, 198);
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(backgroundImage, (int) x - 400, (int) y - 300, 1500, 700, null);
        Font customFontMain = loadCustomFont("src/main/resources/Mediaval_font.ttf", true);
        g.setFont(customFontMain);
        g.setColor(clr);
        Rectangle exitButton = new Rectangle((int) x - 100, (int) y - 150, 200, 50);
        g.drawString("You won!", (int) x - 100, (int) y - 200);
        g.setFont(customFont);
        g.drawString("Exit", exitButton.x + 15, exitButton.y + 30);
        g2d.draw(exitButton);
    }

    /**
     * Loads a custom font from the specified font file.
     *
     * @param fontFileName the path to the font file
     * @param type means if it is a for Game name or for buttons
     * @return the loaded custom font
     */
    private static Font loadCustomFont(String fontFileName, boolean type) {
        try {
            if (!type) {
                Font font = Font.createFont(Font.TRUETYPE_FONT, new File(fontFileName));
                return font.deriveFont(Font.PLAIN, 25); // Adjust the font size as needed
            } else {
                Font font = Font.createFont(Font.TRUETYPE_FONT, new File(fontFileName));
                return font.deriveFont(Font.PLAIN, 50); // Adjust the font size as needed
            }
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
            // Default font
            return new Font("Serif", Font.PLAIN, 25); // Fall back to a default font
        }
    }
}


