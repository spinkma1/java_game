package cz.cvut.fel.pjv.Utils;

import com.google.gson.annotations.Expose;
import cz.cvut.fel.pjv.Objects.ObjectType;

import java.io.Serializable;
import java.util.ArrayList;

public class Inventory implements Serializable {
    @Expose
    private  ArrayList<GameObject> items;

    /**
     * Constructs a new Inventory object.
     * Initializes the list of items as an empty ArrayList.
     */
    public Inventory() {
        items = new ArrayList<>();
    }

    /**
     * Adds a game object to the inventory.
     * If the object is not already present in the inventory, it is added.
     * If the object is currently in the game, its "isInGame" flag is set to false.
     *
     * @param object the game object to be added to the inventory
     */
    public void addItem(GameObject object) {
        if(!items.contains(object)){
            items.add(object);
            if(object.isInGame()){
                object.setInGame(false);
            }
        }

    }


    /**
     * Removes a game object from the inventory.
     *
     * @param object the game object to be removed from the inventory
     */
    public void removeItem(GameObject object) {
        items.remove(object);
    }
    /**
     * Returns the list of items in the inventory.
     *
     * @return the list of items in the inventory
     */
    public ArrayList<GameObject> getItems() {
        return items;
    }
    /**
     * Returns the number of keys in the inventory.
     *
     * @return the number of keys in the inventory
     */
    public int returnSize(){
        int keys=0;
        for(int i=0;i<items.size();i++){
            if(items.get(i).getType()== ObjectType.Key){
                keys++;
            }
        }
        return keys;
    }
    public int returnNumOfDoors(){
        int doors=0;
        for(int i=0;i<items.size();i++){
            if(items.get(i).getType()==ObjectType.Doors){
                doors++;
            }
        }
        return doors;
    }
    /**
     * Returns the number of coins in the inventory.
     *
     * @return the number of coins in the inventory
     */
    public int returnNumOfCoins(){
        int coins=0;
        for(int i=0;i<items.size();i++){
            if(items.get(i).getType()==ObjectType.Coin){
                coins++;
            }
        }
        return coins;
    }
    /**
     * Prints the items in the inventory.
     * This is for debugging and testing.
     */
    public void printItems(){
        System.out.println("----------------");
        for(int i=0;i<items.size();i++){
            System.out.println(items.get(i).getType());
        }
        System.out.println("----------------");
    }
    /**
     * Sets the list of items in the inventory.
     *
     * @param items the list of items to set in the inventory
     */
    public void setItems(ArrayList<GameObject> items) {
        this.items = items;
    }
}

