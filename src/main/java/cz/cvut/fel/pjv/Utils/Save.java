package cz.cvut.fel.pjv.Utils;
import cz.cvut.fel.pjv.Images.ImagesManager;
import cz.cvut.fel.pjv.Controllers.Game;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import java.util.Iterator;

public class Save {
    private final Game game;
    private final float X;
    private final float Y;
    private BufferedImage img;
    private Inventory inv;
    private String date;
    /**
     * Constructs a new Save object.
     *
     * @param game the game instance
     * @param X    the player's X coordinate
     * @param Y    the player's Y coordinate
     */
    public Save(Game game, float X, float Y) {
        this.game=game;
        this.X=X;
        this.Y=Y;
        date=returnDate();

    }
    /**
     * Saves the game state by modifying the game level image and saving it to a file.
     *
     * @throws IOException if an I/O error occurs during the save process
     */
    public void SaveGame() throws IOException {
        ImagesManager manager = new ImagesManager();
        try{
            img=manager.loadImage("/level.png");
        }catch(IOException e){
            e.printStackTrace();
        }

        BufferedImage modifiedImage = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g = modifiedImage.createGraphics();
        g.drawImage(img, 0, 0, null);


        int height=img.getHeight();
        int width= img.getWidth();
        ArrayList<float[]> coords =RewriteLevelFromInv();




        for (float[] coordinate : coords) {
            float x = coordinate[0];
            float y = coordinate[1];
            modifiedImage.setRGB((int)x/32, (int) y/32, Color.BLACK.getRGB());
        }

        for(int i =0;i < height;i++){
            for(int j=0;j< width;j++){
                int color = img.getRGB(i,j);
                int red = (color >> 16) & 0xff;
                int green = (color >> 8) & 0xff;
                int blue = color & 0xff;

                if (red == 0 && green == 0 && blue == 255){
                    modifiedImage.setRGB(i, j, Color.BLACK.getRGB());
                }
            }
        }
        modifiedImage.setRGB((int)X/32, (int)Y/32, new Color(0, 0, 255).getRGB());
        ImageIO.write(modifiedImage, "png", new File("src/main/resources/saves/level_save_"+date+".png"));

    }
    /**
     * Saves the game's inventory to a file.
     *
     * @param inv the player's inventory
     */
    public void SaveInventory(Inventory inv) {
        inv.printItems();

        try (FileOutputStream fileOut = new FileOutputStream("src/main/resources/saves/player_inventory_" + date + ".json");
             ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)) {
            objectOut.writeObject(inv);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    /**
     * Rewrites the game level using the player's inventory and returns the modified coordinates.
     *
     * @return the list of modified coordinates
     */
    private ArrayList<float[]> RewriteLevelFromInv(){
        ArrayList<float[]> coordinates = new ArrayList<>();

        String filePath = "src/main/resources/saves/player_inventory_" + date + ".json";

        try (FileInputStream fileIn = new FileInputStream(filePath);
             ObjectInputStream in = new ObjectInputStream(fileIn)) {

            Object deserializedObject = in.readObject();

            inv = (Inventory) deserializedObject;
        } catch (Exception e) {
            e.printStackTrace();
        }

        Iterator<GameObject> iterator = inv.getItems().iterator();
        while (iterator.hasNext()) {
            GameObject object = iterator.next();
            coordinates.add(new float[]{object.getX(), object.getY()});
        }
        return coordinates;
    }
    /**
     * Returns the current date and time as a formatted string.
     *
     * @return the formatted date and time
     */
    public String returnDate(){
        LocalDateTime currentTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
        return currentTime.format(formatter);
    }
}
