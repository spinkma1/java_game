package cz.cvut.fel.pjv.Utils;

import java.util.logging.Filter;
import java.util.logging.LogRecord;

public class LoggerFilter implements Filter {
    private int count = 0;

    public boolean isLoggable(LogRecord record) {
        count++;
        return count % 40== 0;
    }
}
