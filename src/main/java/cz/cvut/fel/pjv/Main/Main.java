package cz.cvut.fel.pjv.Main;

import cz.cvut.fel.pjv.Controllers.Game;
import cz.cvut.fel.pjv.Window.Window;

/**

 The Main class is the entry point of the game. Game is not resizeble
 */
public class Main {
    public static void main(String[] args) {
        new Window(800, 600, new Game(), "Spider's Fury");
    }
}
