package cz.cvut.fel.pjv.Objects;

import cz.cvut.fel.pjv.Controllers.Controller;
import cz.cvut.fel.pjv.Utils.GameObject;
import cz.cvut.fel.pjv.Images.Textures;
import cz.cvut.fel.pjv.Utils.Inventory;

import java.awt.*;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * The Doors class represents doors in the game.
 * It extends the GameObject class and implements the Serializable interface for saving in json file.
 * Doors are objects that is rendered on the screen and can be interacted with.
 */
public class Doors extends GameObject implements Serializable {

    private final Textures tex;

    /**
     * Constructs a new Doors object with the specified coordinates, texture, and type.
     *
     * @param x    The x-coordinate of the doors.
     * @param y    The y-coordinate of the doors.
     * @param tex  The Textures object representing the doors' texture.
     * @param type The ObjectType of the doors.
     */
    public Doors(float x, float y, Textures tex, ObjectType type) {
        super(x, y, type);
        this.tex = tex;
    }

    /**
     * Updates the state of the doors.
     *
     * @param object The list of GameObjects in the game.
     */
    @Override
    public void tick(LinkedList<GameObject> object) {
        // Doors do not have any update logic
    }

    /**
     * Handles the attack action on the doors.
     *
     * @param flag The attack flag indicating whether the doors are attacking.
     */
    @Override
    public void attack(Boolean flag) {
        // Doors cant attack
    }

    /**
     * Renders the doors on the screen.
     *
     * @param g The Graphics object used for rendering.
     */
    public void render(Graphics g) {
        if (isInGame)
            g.drawImage(tex.getSubimage1(), (int) x, (int) y, null);
    }

    /**
     * Checks if the doors are in the game.
     *
     * @return true if the doors are in the game, false otherwise.
     */
    public boolean isInGame() {
        return isInGame;
    }

    /**
     * Sets the in-game status of the doors.
     *
     * @param inGame The in-game status of the doors.
     */
    public void setInGame(boolean inGame) {
        isInGame = inGame;
    }

    /**
     * Retrieves the bounding rectangle of the doors.
     *
     * @return The Rectangle object representing the hitbox of the doors.
     */
    @Override
    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, 32, 32);
    }

    /**
     * Retrieves the amount of inventory of the doors.
     *
     * @return The amount of inventory of the doors.
     */
    @Override
    public int getInventoryAmount() {
        return 0;
    }

    /**
     * Retrieves the inventory of the doors.
     *
     * @return The Inventory object representing the doors' inventory.
     */
    @Override
    public Inventory getInventory() {
        return null;
    }
}
