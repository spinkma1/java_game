package cz.cvut.fel.pjv.Objects;

import cz.cvut.fel.pjv.Controllers.Controller;
import cz.cvut.fel.pjv.Images.Animations;
import cz.cvut.fel.pjv.Utils.GameObject;
import cz.cvut.fel.pjv.Images.Textures;
import cz.cvut.fel.pjv.Utils.Inventory;

import java.awt.*;
import java.util.LinkedList;
import java.util.logging.Logger;

/**
 * The Player class represents the player character in the game.
 * It extends the GameObject class and provides specific behavior and properties for the player.
 */
public class Player extends GameObject {


    private final int width=32;
    private final int height=32;
    private final float GRAVITY=0.5f;
    private final Controller controller;
    Textures tex;
    private final Animations walk;
    private final Animations walkReverse;
    private final Animations idle;
    private final Animations idle_reverse;
    private final Inventory inv;
    private final Animations dead;
    private final Animations attack;
    private final Animations attack_reverse;

    private boolean direction=true;
    boolean isAttacking=false;
    private boolean hasKey=false;

    private static final Logger logger = Logger.getLogger(Player.class.getName());

    private Boolean IsLoggerOn=false;

    public Player(float x, float y, Textures tex,Textures tex_reverse,ObjectType type,Controller controller,Inventory inv){
        super(x,y,type);
        this.controller=controller;
        this.tex=tex;
        this.inv=inv;

        walk= new Animations(5,tex.getSubimage2(),tex.getSubimage13(),tex.getSubimage3(),tex.getSubimage6());
        walkReverse = new Animations(5,tex_reverse.getSubimage2(),tex_reverse.getSubimage13(),tex_reverse.getSubimage3(),tex_reverse.getSubimage6());
        idle = new Animations(10,tex.getSubimage2(),tex.getSubimage5(),tex.getSubimage3());
        idle_reverse = new Animations(10,tex_reverse.getSubimage2(),tex_reverse.getSubimage8(),tex_reverse.getSubimage3());
        dead = new Animations(10,tex.getSubimage12());

        attack = new Animations(12,tex.getSubimage2(),tex.getSubimage9(),tex.getSubimage10(),tex.getSubimage11(),tex.getSubimage14());
        attack_reverse = new Animations(12,tex_reverse.getSubimage2(),tex_reverse.getSubimage9(),tex_reverse.getSubimage10(),tex_reverse.getSubimage11(),tex_reverse.getSubimage14());
    }

    /**
     * Update the player's position and handle collision and animation.
     *
     * @param object The list of game objects in the game.
     */
    @Override
    public void tick(LinkedList<GameObject> object) {
        x+=Xcoor;
        y+=Ycoor;

        if (FALLING || JUMPING ){
            Ycoor+=GRAVITY;
        }
        if(!hasKey && inv.returnNumOfDoors()< inv.returnSize()){
            hasKey=true;
        }
        collision(object);
        walk.Animate();
        walkReverse.Animate();
        idle.Animate();
        idle_reverse.Animate();
        attack_reverse.Animate();
        attack.Animate();
    }
    /**
     * Handle the player's attack.
     *
     * @param flag A boolean indicating whether the player is attacking or not.
     */
    public void attack(Boolean flag) {
        if (flag) {
            isAttacking=true;
        }else if(!flag){
            isAttacking=false;
        }

    }
    /**
     * Handles collision with other game objects.
     *
     * @param o The list of game objects in the game.
     */
    private void collision(LinkedList<GameObject> o){
        for(int i=0;i<o.size();i++) {
            controller.temObject = o.get(i);
            if (controller.temObject.getType() == ObjectType.Block) {
                blockCollision(controller.temObject);
            } else if (controller.temObject.getType() == ObjectType.Key) {
                keyCollision(controller.temObject);
            } else if (controller.temObject.getType() == ObjectType.Spider) {
                spiderCollision(controller.temObject);
            } else if (controller.temObject.getType() == ObjectType.Coin) {
                coinCollision(controller.temObject);
            } else if (controller.temObject.getType() == ObjectType.Chest) {
                chestCollision(controller.temObject);
            } else if (controller.temObject.getType() == ObjectType.Doors) {
                doorsCollision(controller.temObject);
            }
        }
        if(isAttacking){
            for(int i=0;i<controller.object.size();i++){
                controller.temObject=controller.object.get(i);
                if(controller.temObject.getType()==ObjectType.Spider){
                    if (getBoundsLeft().intersects(controller.temObject.getBounds())&& isAttacking) {
                        if(IsLoggerOn){
                            logger.info(this.getType() + " killed a "+controller.temObject.getType()); //logger
                        }
                        controller.removeObject(controller.temObject);
                    }else if(getBoundsRight().intersects(controller.temObject.getBounds()) && isAttacking){
                        if(IsLoggerOn){
                            logger.info(this.getType() + " killed a "+controller.temObject.getType()); //logger
                        }
                        controller.removeObject(controller.temObject);
                    }
                }
            }
        }


    }

    /**
     * Render the player's imgage and animation.
     *
     * @param g The Graphics object to draw on.
     */
    public void render(Graphics g) {
        if (!isAttacking) {
            if (Xcoor > 0) {
                walk.drawAnimation(g, (int) x, (int) y);
                direction=true;
            } else if (Xcoor < 0) {
                walkReverse.drawAnimation(g, (int) x, (int) y);
                direction=false;
            } else if(direction) {
                idle.drawAnimation(g, (int) x, (int) y);
            }else if(!direction){
                idle_reverse.drawAnimation(g, (int) x, (int) y);
            }
        }
        if(isAttacking && direction){
            attack.drawAnimation(g, (int) x, (int) y);
        }else if(isAttacking && !direction){
            attack_reverse.drawAnimation(g, (int) x, (int) y);
        }


    }


    private void blockCollision(GameObject object){
        if(getBounds().intersects(object.getBounds())){
            Ycoor=0;
            FALLING=false;
            JUMPING=false;
            y=object.getY()-height;
        }else{
            FALLING=true;
        }
        if(getBoundsTop().intersects(object.getBounds())){
            Ycoor=0;
            y=object.getY()+32;
        } else if (getBoundsLeft().intersects(object.getBounds())) {
            x=object.getX()+width;
        }else if(getBoundsRight().intersects(object.getBounds())){
            x=object.getX()-width;
        }

    }
    private void keyCollision(GameObject object){
        if(getBounds().intersects(object.getBounds())){
            if(IsLoggerOn){
                logger.info(this.getType() + " found a "+object.getType()); //logger
            }
            hasKey=true;
            inv.addItem(object);
        }
        if(getBoundsTop().intersects(object.getBounds())){
            if(IsLoggerOn){
                logger.info(this.getType() + " found a "+object.getType()); //logger
            }
            hasKey=true;
            inv.addItem(object);
        } else if (getBoundsLeft().intersects(object.getBounds())) {
            if(IsLoggerOn){
                logger.info(this.getType() + " found a "+object.getType()); //logger
            }
            hasKey=true;
            inv.addItem(object);
        }else if(getBoundsRight().intersects(object.getBounds())){
            if(IsLoggerOn){
                logger.info(this.getType() + " found a "+object.getType()); //logger
            }
            hasKey=true;
            inv.addItem(object);
        }
    }
    private void spiderCollision(GameObject object){
        if(object.getBounds().intersects(getBoundsTop())&& !isAttacking){
            if(IsLoggerOn){
                logger.info(this.getType() + " was killed by a"+object.getType()); //logger
            }
            dead.Animate();
            controller.removeObject(this);
        } else if (object.getBounds().intersects(getBoundsLeft())&&!isAttacking) {
            if(IsLoggerOn){
                logger.info(this.getType() + " was killed by a"+object.getType()); //logger
            }
            dead.Animate();
            controller.removeObject(this);
        }else if(object.getBounds().intersects(getBoundsRight())&&!isAttacking){
            if(IsLoggerOn){
                logger.info(this.getType() + " was killed by a"+object.getType()); //logger
            }
            dead.Animate();
            controller.removeObject(this);
        }
    }
    private  void coinCollision(GameObject object){
        if(object.getBounds().intersects(getBoundsTop())){
            if(IsLoggerOn){
                logger.info(this.getType() + " found a "+object.getType()); //logger
            }
            inv.addItem(object);
            controller.removeObject(object);
        } else if (object.getBounds().intersects(getBoundsLeft())) {
            if(IsLoggerOn){
                logger.info(this.getType() + " found a "+object.getType()); //logger
            }
            inv.addItem(object);
            controller.removeObject(object);
        }else if(object.getBounds().intersects(getBoundsRight())){
            if(IsLoggerOn){
                logger.info(this.getType() + " found a "+object.getType()); //logger
            }
            inv.addItem(object);
            controller.removeObject(object);
        }else if(object.getBounds().intersects(getBounds())){
            if(IsLoggerOn){
                logger.info(this.getType() + " found a "+object.getType()); //logger
            }

            inv.addItem(object);
            controller.removeObject(object);
        }
    }
    private void chestCollision(GameObject object){
        if(object.getBounds().intersects(getBoundsTop())){
            if(IsLoggerOn){
                logger.info(this.getType() + " won the game - "+object.getType()+"is on "+object.getX()+", "+object.getY()); //logger
            }
            inv.addItem(object);
            controller.removeObject(object);
        } else if (object.getBounds().intersects(getBoundsLeft())) {
            if(IsLoggerOn){
                logger.info(this.getType() + " won the game - "+object.getType()+"is on "+object.getX()+", "+object.getY()); //logger
            }
            inv.addItem(object);
            controller.removeObject(object);
        }else if(object.getBounds().intersects(getBoundsRight())){
            if(IsLoggerOn){
                logger.info(this.getType() + " won the game - "+object.getType()+"is on "+object.getX()+", "+object.getY()); //logger
            }
            inv.addItem(object);
            controller.removeObject(object);
        }else if(object.getBounds().intersects(getBounds())){
            if(IsLoggerOn){
                logger.info(this.getType() + " won the game - "+object.getType()+"is on "+object.getX()+", "+object.getY()); //logger
            }
            inv.addItem(object);
            controller.removeObject(object);
        }
    }
    public void doorsCollision(GameObject object){
        if(object.getBounds().intersects(getBoundsTop())){
            if(hasKey){
                inv.addItem(object);
                controller.removeObject(object);
                hasKey=false;
            }else{
                Ycoor=0;
                y=object.getY()+32;
            }
        } else if (object.getBounds().intersects(getBoundsLeft())) {
            if(hasKey){
                inv.addItem(object);
                controller.removeObject(object);
                hasKey=false;
            }else{
                x=object.getX()+width;
            }
        }else if(object.getBounds().intersects(getBoundsRight())){
            if(hasKey){
                inv.addItem(object);
                controller.removeObject(object);
                hasKey=false;
            }else{
                x=object.getX()-width;
            }
        }else if(object.getBounds().intersects(getBounds())){
            if(hasKey){
                inv.addItem(object);
                controller.removeObject(object);
                hasKey=false;
            }else{
                Ycoor=0;
                FALLING=false;
                JUMPING=false;
                y=object.getY()-height;
            }
        }

    }
    public int getInventoryAmount(){
        return inv.returnSize();
    }

    /**
     * Get the player's inventory.
     *
     * @return The inventory object.
     */
    @Override
    public Inventory getInventory() {
        return null;
    }
    public void setHasKey(Boolean bool){
        this.hasKey=bool;
    }


    /**
     * Get the boundaries of the player's img for collision detection.
     *
     * @return The rectangle representing the boundaries of the player's sprite.
     */
    public Rectangle getBounds() {
        return new Rectangle((int)x+7,(int)y+27, width -15, 5);
    }

    /**
     * Get the top boundary of the player's sprite for collision detection.
     *
     * @return The rectangle representing the top boundary of the player's sprite.
     */
    public Rectangle getBoundsTop() {
        return new Rectangle((int)x+7,(int)y, width -15, 5);
    }

    /**
     * Get the left boundary of the player's sprite for collision detection.
     *
     * @return The rectangle representing the left boundary of the player's sprite.
     */
    public Rectangle getBoundsLeft() {
        return new Rectangle((int)x,(int)y+5,5, height -10);
    }
    /**
     * Get the right boundary of the player's sprite for collision detection.
     *
     * @return The rectangle representing the right boundary of the player's sprite.
     */
    public Rectangle getBoundsRight() {
        return new Rectangle((int)x+26,(int)y+5, 5, height -10);
    }

    public Boolean getLoggerOn() {
        return IsLoggerOn;
    }
    public void setLoggerOn(boolean loggerOn) {
        this.IsLoggerOn=loggerOn;
    }

}
