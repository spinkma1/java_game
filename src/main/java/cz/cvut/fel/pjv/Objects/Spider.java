package cz.cvut.fel.pjv.Objects;

import cz.cvut.fel.pjv.Controllers.Controller;
import cz.cvut.fel.pjv.Utils.GameObject;
import cz.cvut.fel.pjv.Images.Textures;
import cz.cvut.fel.pjv.Utils.Inventory;

import java.awt.*;
import java.util.LinkedList;

/**
 * The Spider class represents a spider object in the game.
 * It extends the GameObject class and implements specific behavior for the spider.
 */
public class Spider extends GameObject {
    private final int width = 32;
    private final int height = 32;
    private final Textures tex;
    private final Controller controller;
    private boolean direction = true;

    /**
     * Constructs a new Spider object.
     *
     * @param x          the x-coordinate of the spider's position
     * @param y          the y-coordinate of the spider's position
     * @param tex        the textures for rendering the spider
     * @param type       the type of the spider object
     * @param controller the game controller for interaction
     */
    public Spider(float x, float y, Textures tex, ObjectType type, Controller controller) {
        super(x, y, type);
        this.tex = tex;
        this.controller = controller;
    }

    /**
     * Updates the spider's position and checks for collisions with other objects.
     *
     * @param o the list of all game objects
     */
    @Override
    public void tick(LinkedList<GameObject> o) {
        if (direction) {
            x++;
        } else if (!direction) {
            x--;
        }
        collision(o);
    }

    /**
     * Performs the spider's attack action.
     *
     * @param flag true if the spider is attacking, false otherwise
     */
    @Override
    public void attack(Boolean flag) {
        // Spider does not perform any attacks
    }

    /**
     * Checks for collisions between the spider and other game objects.
     *
     * @param o the list of all game objects
     */
    private void collision(LinkedList<GameObject> o) {
        for (int i = 0; i < o.size(); i++) {
            controller.temObject = o.get(i);
            if (controller.temObject.getType() == ObjectType.Block) {
                if (getBounds().intersects(controller.temObject.getBounds())) {
                    y = controller.temObject.getY() - height;
                }
                if (getBoundsTop().intersects(controller.temObject.getBounds())) {
                    Ycoor = 0;
                    y = controller.temObject.getY() + 32;
                } else if (getBoundsLeft().intersects(controller.temObject.getBounds())) {
                    x = controller.temObject.getX() + width;
                    direction = true;
                } else if (getBoundsRight().intersects(controller.temObject.getBounds())) {
                    x = controller.temObject.getX() - width;
                    direction = false;
                }
            }
            if (controller.temObject.getType() == ObjectType.Player) {
                if (getBoundsTop().intersects(controller.temObject.getBounds())) {
                    controller.removeObject(this);
                }
            }
        }
    }

    /**
     * Renders the spider on the screen.
     *
     * @param g the graphics context
     */
    public void render(Graphics g) {
        if (direction) {
            g.drawImage(tex.getSubimage1(), (int) x, (int) y, null);
        } else {
            g.drawImage(tex.getSubimage2(), (int) x, (int) y, null);
        }
    }

    /**
     * Returns the bounding rectangle for the spider's collision detection.
     *
     * @return the bounding rectangle
     */
    public Rectangle getBounds() {
        return new Rectangle((int) x + 7, (int) y + 17, width - 15, 15);
    }

    /**
     * Returns the top bounding rectangle for the spider's collision detection.
     *
     * @return the top bounding rectangle
     */
    public Rectangle getBoundsTop() {
        return new Rectangle((int) x + 7, (int) y, width - 15, 5);
    }

    /**
     * Returns the left bounding rectangle for the spider's collision detection.
     *
     * @return the left bounding rectangle
     */
    public Rectangle getBoundsLeft() {
        return new Rectangle((int) x, (int) y + 5, 5, height - 10);
    }

    /**
     * Returns the right bounding rectangle for the spider's collision detection.
     *
     * @return the right bounding rectangle
     */
    public Rectangle getBoundsRight() {
        return new Rectangle((int) x + 26, (int) y + 5, 5, height - 10);
    }

    /**
     * Returns the amount of inventory items held by the spider.
     *
     * @return the inventory amount
     */
    @Override
    public int getInventoryAmount() {
        return 0;
    }

    /**
     * Returns the inventory of the spider.
     *
     * @return the spider's inventory
     */
    @Override
    public Inventory getInventory() {
        return null;
    }
}

