package cz.cvut.fel.pjv.Objects;

/**
 * The ObjectType enum represents the different types of game objects.
 */
public enum ObjectType {
    Player,
    Block,
    Key,
    Spider,
    Doors,
    Coin,
    Chest
}
