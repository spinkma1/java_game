package cz.cvut.fel.pjv.Objects;

import cz.cvut.fel.pjv.Utils.GameObject;
import cz.cvut.fel.pjv.Images.Textures;
import cz.cvut.fel.pjv.Utils.Inventory;

import java.awt.*;
import java.util.LinkedList;

/**

 The Block class represents a block object in the game.
 */
public class Block extends GameObject {
    private final int texture;
    private final Textures tex;

    /**

     Constructs a Block object with the specified position, texture, texture image, and object type.
     @param x the x-coordinate of the block
     @param y the y-coordinate of the block
     @param texture the texture index of the block, indicates what block texture to use
     @param tex the texture image of the block
     @param type the object type of the block
     */
    public Block(float x, float y, int texture, Textures tex, ObjectType type) {
        super(x, y, type);
        this.texture = texture;
        this.tex = tex;
    }
    /**

     Updates the block's state.
     @param object the list of game objects
     */
    public void tick(LinkedList<GameObject> object) {
        // Empty implementation, as the block does not require any specific update logic
    }
    /**

     Performs an attack action.
     @param flag the flag indicating whether the attack is active
     */
    @Override
    public void attack(Boolean flag) {
        // Empty implementation, as the block does not perform attacks
    }
    /**

     Renders the block on the screen.
     @param g the graphics context
     */
    public void render(Graphics g) {
        if (texture == 0) { // Cobblestone texture
            g.drawImage(tex.getSubimage1(), (int) x, (int) y, null);
        }
        if (texture == 1) { // Stone texture
            g.drawImage(tex.getSubimage2(), (int) x, (int) y, null);
        }
    }
    /**

     Retrieves the bounding rectangle of the block.
     @return the bounding rectangle
     */
    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, 32, 32);
    }
    /**

     Retrieves the amount of the block in the game's inventory.
     @return the amount of the block
     */
    @Override
    public int getInventoryAmount() {
        return 0;
    }
    /**

     Retrieves the inventory of the block.
     @return the inventory of the block
     */
    @Override
    public Inventory getInventory() {
        return null;
    }
}
