package cz.cvut.fel.pjv.Objects;

import cz.cvut.fel.pjv.Utils.GameObject;
import cz.cvut.fel.pjv.Images.Textures;
import cz.cvut.fel.pjv.Utils.Inventory;

import java.awt.*;
import java.util.LinkedList;

/**

 The Chest class represents a chest object in the game.
 */
public class Chest extends GameObject {
    private final Textures tex;
    private boolean isInGame;

    /**

     Constructs a Chest object with the specified position, texture image, and object type.
     @param x the x-coordinate of the chest
     @param y the y-coordinate of the chest
     @param tex the texture image of the chest
     @param type the object type of the chest
     */
    public Chest(float x, float y, Textures tex, ObjectType type) {
        super(x, y, type);
        this.tex = tex;
        isInGame = true;
    }
    /**

     Updates the chest's state.
     @param object the list of game objects
     */
    public void tick(LinkedList<GameObject> object) {
        // Empty implementation, as the chest does not require any specific update logic
    }
    /**

     Performs an attack action.
     @param flag the flag indicating whether the attack is active
     */
    public void attack(Boolean flag) {
        // Empty implementation, as the chest does not perform attacks
    }
    /**
     Renders the chest on the screen.
     @param g the graphics context
     */
    public void render(Graphics g) {
        if (isInGame) { //checks if the chest is in the game
            g.drawImage(tex.getSubimage1(), (int) x, (int) y, null);
        }
    }
    /**

     Retrieves the bounding rectangle of the chest.
     @return the bounding rectangle
     */

    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, 32, 32);
    }
    /**

     Checks if the chest is in the game.
     @return true if the chest is in the game, false otherwise
     */
    public boolean isInGame() {
        return isInGame;
    }
    /**

     Sets the in-game status of the chest.
     @param inGame the in-game status of the chest
     */
    public void setInGame(boolean inGame) {
        isInGame = inGame;
    }
    /**

     Retrieves the amount of the chest in the inventory.
     @return the amount of the chest
     */

    public int getInventoryAmount() {
        return 0;
    }
    /**

     Retrieves the inventory of the chest.
     @return the inventory of the chest
     */

    public Inventory getInventory() {
        return null;
    }
}