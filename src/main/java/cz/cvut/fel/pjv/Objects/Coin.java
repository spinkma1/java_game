package cz.cvut.fel.pjv.Objects;

import cz.cvut.fel.pjv.Utils.GameObject;
import cz.cvut.fel.pjv.Images.Textures;
import cz.cvut.fel.pjv.Utils.Inventory;

import java.awt.*;
import java.io.Serializable;
import java.util.LinkedList;

/**

 The Coin class represents a coin object in the game.
 */
public class Coin extends GameObject implements Serializable {
    private final Textures tex;
    private boolean isInGame;

    /**

     Constructs a Coin object with the specified position, texture image, and object type.
     @param x the x-coordinate of the coin
     @param y the y-coordinate of the coin
     @param tex the texture image of the coin
     @param type the object type of the coin
     */
    public Coin(float x, float y, Textures tex, ObjectType type) {
        super(x, y, type);
        this.tex = tex;
        isInGame = true;
    }
    /**

     Updates the coin's state.
     @param object the list of game objects
     */
    @Override
    public void tick(LinkedList<GameObject> object) {
// Empty implementation, as the coin does not require any specific update logic
    }
    /**

     Performs an attack action.
     @param flag the flag indicating whether the attack is active
     */
    @Override
    public void attack(Boolean flag) {
        // Empty implementation, as the coin does not perform attacks
    }
    /**
     Renders the coin on the screen.
     @param g the graphics context
     */
    @Override
    public void render(Graphics g) {
        if (isInGame) {
            g.drawImage(tex.getSubimage1(), (int) x, (int) y, null);
        }
    }
    /**
     Retrieves the bounding rectangle of the coin.
     @return the bounding rectangle
     */
    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, 32, 32);
    }
    /**
     Checks if the coin is in the game.
     @return true if the coin is in the game, false otherwise
     */
    public boolean isInGame() {
        return isInGame;
    }
    /**

     Sets the in-game status of the coin.
     @param inGame the in-game status of the coin
     */
    public void setInGame(boolean inGame) {
        isInGame = inGame;
    }
    /**

     Retrieves the amount of the coin in the inventory.
     @return the amount of the coin
     */
    public int getInventoryAmount() {
        return 0;
    }
    /**

     Retrieves the inventory of the coin.
     @return the inventory of the coin
     */
    public Inventory getInventory() {
        return null;
    }
}
