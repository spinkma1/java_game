package cz.cvut.fel.pjv.Objects;

import cz.cvut.fel.pjv.Utils.GameObject;
import cz.cvut.fel.pjv.Images.Textures;
import cz.cvut.fel.pjv.Utils.Inventory;

import java.awt.*;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * The Key class represents a key in the game.
 * It extends the GameObject class and implements the Serializable interface.
 * Keys are objects that can be collected by the player.
 */
public class Key extends GameObject implements Serializable {

    private final Textures tex;
    private boolean isInGame = true;

    /**
     * Constructs a new Key object with the specified coordinates, texture, and type.
     *
     * @param x    The x-coordinate of the key.
     * @param y    The y-coordinate of the key.
     * @param tex  The Textures object representing the key's texture.
     * @param type The ObjectType of the key.
     */
    public Key(float x, float y, Textures tex, ObjectType type) {
        super(x, y, type);
        this.tex = tex;
    }

    /**
     * Updates the state of the key.
     *
     * @param object The list of GameObjects in the game.
     */
    @Override
    public void tick(LinkedList<GameObject> object) {
        // Keys do not have any update logic for now
    }

    /**
     * Handles the attack action on the key.
     *
     * @param flag The attack flag indicating whether the key is attacking.
     */
    @Override
    public void attack(Boolean flag) {
        // Keys can't attack
    }

    /**
     * Renders the key on the screen.
     *
     * @param g The Graphics object used for rendering.
     */
    public void render(Graphics g) {
        if (isInGame)
            g.drawImage(tex.getSubimage1(), (int) x, (int) y, null);
    }

    /**
     * Checks if the key is in the game.
     *
     * @return true if the key is in the game, false otherwise.
     */
    public boolean isInGame() {
        return isInGame;
    }

    /**
     * Sets the in-game status of the key.
     *
     * @param inGame The in-game status of the key.
     */
    public void setInGame(boolean inGame) {
        isInGame = inGame;
    }

    /**
     * Retrieves the bounding rectangle of the key.
     *
     * @return The Rectangle object representing the bounds of the key.
     */
    @Override
    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, 32, 32);
    }

    /**
     * Retrieves the amount of inventory of the key.
     *
     * @return The amount of inventory of the key.
     */
    @Override
    public int getInventoryAmount() {
        return 0;
    }

    /**
     * Retrieves the inventory of the key.
     *
     * @return The Inventory object representing the key's inventory.
     */
    @Override
    public Inventory getInventory() {
        return null;
    }
}

