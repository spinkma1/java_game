package cz.cvut.fel.pjv.Controllers;
import cz.cvut.fel.pjv.Objects.ObjectType;
import cz.cvut.fel.pjv.Utils.GameStates;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * The KeyInputs class handles keyboard input events for the game.
 */
public class KeyInputs extends KeyAdapter {
    private final Controller controller;
    private final GameStates gameStates;
    private final Game game;
    private boolean flag_paused =false;
    private boolean attacking=false;

    /**
     * Constructs a KeyInputs object.
     *
     * @param controller  the controller instance
     * @param gameStates  the gameStates instance that handles GameStaes
     * @param game        the game instance
     */
    public KeyInputs(Controller controller, GameStates gameStates, Game game) {
        this.controller = controller;
        this.gameStates = gameStates;
        this.game=game;
    }

    /**
     * Invoked when a key is pressed.
     *
     * @param e  the KeyEvent object that contains the event information
     */
    public void keyPressed(KeyEvent e) {
        for (int i = 0; i < controller.object.size(); i++) {
            controller.temObject = controller.object.get(i);
            if (controller.temObject.getType() == ObjectType.Player) {
                int key = e.getKeyCode();
                switch (key) {
                    case KeyEvent.VK_W:
                        if (!controller.object.get(i).isJUMPING()) {
                            controller.object.get(i).setYcoor(-10);
                            controller.object.get(i).setJUMPING(true);
                        }
                        if(attacking){
                            controller.temObject.attack(true);
                        }
                        break;
                    case KeyEvent.VK_A:
                        controller.temObject.setXcoor(-5);
                        if(attacking){
                            controller.temObject.attack(true);
                        }
                        break;
                    case KeyEvent.VK_S:
                        controller.temObject.setYcoor(5);
                        if(attacking){
                            controller.temObject.attack(true);
                        }
                        break;
                    case KeyEvent.VK_ESCAPE:
                        if(!flag_paused){
                            controller.object.get(i).setXcoor(0);
                            controller.object.get(i).setYcoor(0);
                            game.pauseGame();
                            flag_paused=true;
                        }else if(flag_paused){
                            game.unpauseGame();
                            flag_paused=false;
                        }
                        break;
                    case KeyEvent.VK_D:
                        controller.temObject.setXcoor(5);
                        if(attacking){
                            controller.temObject.attack(true);
                        }
                        break;
                    case KeyEvent.VK_SPACE:
                        controller.temObject.attack(true);
                        attacking=true;
                        break;
                }
            }
        }
    }


    /**
     * Invoked when a key is released.
     *
     * @param e  the KeyEvent object that contains the event information
     */
    public void keyReleased (KeyEvent e){
        for (int i = 0; i < controller.object.size(); i++) {
            controller.temObject = controller.object.get(i);
            if (controller.temObject.getType() == ObjectType.Player) {
                int key = e.getKeyCode();
                switch (key) {
                    case KeyEvent.VK_W:
                        controller.object.get(i).setYcoor(0);
                        break;
                    case KeyEvent.VK_A:
                        controller.object.get(i).setXcoor(0);
                        break;
                    case KeyEvent.VK_S:
                        controller.object.get(i).setYcoor(0);
                        break;
                    case KeyEvent.VK_D:
                        controller.object.get(i).setXcoor(0);
                        break;
                    case KeyEvent.VK_SPACE:
                        controller.temObject.attack(false);
                        attacking=false;
                        break;
                }
            }
        }

    }


}





