package cz.cvut.fel.pjv.Controllers;

import cz.cvut.fel.pjv.Objects.ObjectType;
import cz.cvut.fel.pjv.Objects.Player;
import cz.cvut.fel.pjv.Utils.GameObject;
import cz.cvut.fel.pjv.Utils.GameStates;

import java.awt.*;
import java.util.LinkedList;

/**
 * The Controller class manages the game objects and provides methods for adding, removing, updating, and rendering them.
 */
public class Controller {

    /**
     * The linked list of game objects.
     */
    public LinkedList<GameObject> object = new LinkedList<GameObject>();

    /**
     * The temporary game object used for iteration.
     */
    public GameObject temObject;

    /**
     * Indicates whether the player is alive.
     */
    private boolean isPlayerAlive = true;

    /**
     * The last known x-coordinate of the player.
     */
    private float last_x;

    /**
     * The last known y-coordinate of the player.
     */
    private float last_y;

    /**
     * The x-coordinate of the chest.
     */
    private float chest_x;

    /**
     * The y-coordinate of the chest.
     */
    private float chest_y;

    /**
     * Indicates whether the game has been won.
     */
    private boolean isItWon = false;

    /**
     * Adds a game object to the list of objects.
     *
     * @param object the game object to be added
     */
    public void addObject(GameObject object) {
        this.object.add(object);
    }

    /**
     * Removes a game object from the list of objects.
     *
     * @param object the game object to be removed
     */
    public void removeObject(GameObject object) {
        this.object.remove(object);
    }

    /**
     * Updates the game state by iterating over the list of objects and calling their tick() method.
     */
    public void tick() {
        int x = 0;
        int y = 0;
        for (int i = 0; i < object.size(); i++) {
            temObject = object.get(i);
            if (temObject.getType() == ObjectType.Player && object.size() != 0) {
                last_x = temObject.getX();
                last_y = temObject.getY();
                x++;
            }
            if (temObject.getType() == ObjectType.Chest) {
                chest_x = temObject.getX();
                chest_y = temObject.getY();
                y++;
            }
                temObject.tick(object);
        }
        if (x == 0) {
            isPlayerAlive = false;
        }
        if (y == 0) {
            isItWon = true;
        }
    }

    /**
     * Renders the game objects and additional screens based on the game state.
     *
     * @param g           the Graphics object to render on
     * @param gameStates  the GameStates object that manages the game's states
     */
    public void render(Graphics g, GameStates gameStates) {
        for (int i = 0; i < object.size(); i++) {
            temObject = object.get(i);
            temObject.render(g);
        }
        if (!isPlayerAlive) {
            gameStates.renderDeathScene(g, last_x, last_y);
        }
        if (isItWon) {
            gameStates.renderWinScreen(g, chest_x, chest_y);
        }
    }
}





