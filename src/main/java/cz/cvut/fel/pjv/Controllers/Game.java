package cz.cvut.fel.pjv.Controllers;

import cz.cvut.fel.pjv.Images.ImagesManager;
import cz.cvut.fel.pjv.Images.TexturesManager;
import cz.cvut.fel.pjv.Objects.*;
import cz.cvut.fel.pjv.Utils.*;
import cz.cvut.fel.pjv.Objects.ObjectType;
import cz.cvut.fel.pjv.Window.Camera;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Date;
import java.util.logging.Logger;



/**
 * The Game class represents the main game loop and manages the game state, rendering, and updating of game objects.
 */
public class Game extends Canvas implements Runnable{
    /**
     * The choices for game states.
     */
    public enum STATE{
        MENU,GAME
    }

    private static STATE State = STATE.MENU;
    private GameStates gameStates;
    private Thread thread;
    private boolean running = false;
    private static boolean isPaused = false;
    private static boolean Gameover=false;
    GameObject object;
    private Controller controller;
    public static int WIDTH;
    public static int HEIGHT;
    private Camera camera;
    private Player player;

    private BufferedImage lvl=null,moon=null;
    TexturesManager texturesManager;
    private Inventory inv;


    private String level_name="";
    private ImagesManager manager=new ImagesManager();



    private boolean loggersOn =false;
    private static final Logger LOGGER = Logger.getLogger(Game.class.getName());

    private boolean LevelLoaded=false;
    private Thread clockThread;
    private boolean clockRunning = false;
    private long startTime;
    /**
     * Creates a new instance of the Game class.
     */
    public Game(){
    }
    /**
     * Initializes the game.
     */
    private void init()  {

        WIDTH=getWidth();
        HEIGHT=getHeight();

        texturesManager=new TexturesManager();
        try{
            lvl=manager.loadImage("/level.png");
            moon=manager.loadImage("/moon.png"); //background img
        }catch(IOException e){
            e.printStackTrace();
        }

        controller = new Controller();
        gameStates = new GameStates(this);

        camera= new Camera(0,0,this);
        this.addKeyListener(new KeyInputs(controller, gameStates,this));
        this.addMouseListener(new MouseInputs(this, gameStates,controller));
        LOGGER.setFilter(new LoggerFilter());
    }
    /**
     * Starts the game thread.
     */
    public synchronized void start(){
        running=true;
        thread= new Thread(this);
        thread.start();

        clockRunning = true;
        clockThread = new Thread(this::clockLoop);
        clockThread.start();
        startTime = System.currentTimeMillis();
    }

    /**
     * The main game loop.
     */
    public void run(){
        init();
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int updates = 0;
        int frames = 0;
        while(running){
            long now = System.nanoTime();
            delta += (now - lastTime) / ns; //SOURCE : https://www.youtube.com/watch?v=Zh7YiiEuJFw&list=PLWms45O3n--54U-22GDqKMRGlXROOZtMx&index=2&ab_channel=RealTutsGML
            lastTime = now;
            while(delta >= 1){
                tick();
                updates++;
                delta--;
            }
            render();
            frames++;

            if(System.currentTimeMillis() - timer > 1000){
                timer += 1000;
                frames = 0;
                updates = 0;
            }
        }
    }
    /**
     * Renders the game.
     */
    private void render() {
        BufferStrategy bs = this.getBufferStrategy();
        if(bs !=null){
            Graphics g= bs.getDrawGraphics();
            Graphics2D g2d = (Graphics2D) g;

            if(State==STATE.GAME){
                if(!LevelLoaded){
                    LoadInventory();
                    CreateLevel();
                    LevelLoaded=true;
                    tick();
                }
                if(!Gameover) {
                    if (!isPaused) {
                        g.setColor(new Color(6, 50, 89)); //background of the game
                        g.fillRect(0,0,getWidth(),getHeight());
                        g.drawImage(moon,150,0,this);
                        g2d.translate((int) camera.getX(), (int) camera.getY());
                        g.drawRect(0, 0, 150, 150);


                        g.setFont(new Font("Serif", Font.PLAIN, 25));
                        g.setColor(Color.WHITE);
                        int x = -(int) camera.getX() + 10;
                        int y = 20 - (int) camera.getY() + 10; //camera movement
                        g.drawString("Keys: " + player.getInventoryAmount(), x, y);
                        g.drawString("Coins: " + inv.returnNumOfCoins(), x, y+30);
                        g.drawString("Remaining time: " + getRemainingTime(), x, y+60);

                        controller.render(g, gameStates); //rendering all objects
                        g2d.translate((int) -camera.getX(), (int) -camera.getY());
                    } else if (isPaused && State == STATE.GAME) {
                        gameStates.renderPause(g);
                    }
                }
            }else if(State == STATE.MENU){
                gameStates.render(g);
            }
            g.dispose();
            bs.show();
        }else{
            this.createBufferStrategy(3);
        }

    }
    /**
     * Updates the game logic.
     */
    private void tick() {
        if(LevelLoaded==true){
            controller.tick();
            for (int i = 0; i < controller.object.size(); i++) {
                controller.temObject = controller.object.get(i);
                if (controller.temObject.getType() == ObjectType.Player) {
                    camera.update(controller.object.get(i));
                    if(loggersOn){
                        player.setLoggerOn(true);
                        LOGGER.info("Player position: " + controller.temObject.getX() + ", " + controller.temObject.getY()); //logger
                    }
                }
            }
        }
    }
    /**
     * Creates the game level based on the loaded image/based on user's choice of level.
     */
    public void CreateLevel(){
        BufferedImage img = null;
        if(level_name==""){
            try{
                img=manager.loadImage("/level.png"); //loading default save
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            try{
                img=manager.loadImage("/saves/"+level_name); //loading save
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        int height=img.getHeight();
        int width= img.getWidth();

        for(int i =0;i < height;i++){
            for(int j=0;j< width;j++){
                int color = img.getRGB(i,j); //reading RGB code of every pixel in img, then deciding what to do
                int red = (color >> 16) & 0xff;
                int green = (color >> 8) & 0xff;
                int blue = color & 0xff;

                if (red == 255 && green == 255 && blue == 255) {
                    controller.addObject(new Block(i*32,j*32,0,texturesManager.getBlocks(),ObjectType.Block));
                }else if(red == 0 && green == 0 && blue == 255){
                    player=new Player(i*32,j*32,texturesManager.getPlayer(),texturesManager.getPlayer_reverse(),ObjectType.Player,controller,inv);
                    controller.addObject(player);
                }else if (red == 255 && green == 0 && blue == 255){
                    controller.addObject(new Key(i*32,j*32,texturesManager.getKeys(),ObjectType.Key));
                }else if (red == 100 && green == 100 && blue == 100){
                    controller.addObject(new Block(i*32,j*32,1,texturesManager.getBlocks(),ObjectType.Block));
                }else if (red == 0 && green == 255 && blue == 0){
                    controller.addObject(new Spider(i*32,j*32,texturesManager.getSpider(),ObjectType.Spider,controller));
                }else if (red == 255 && green == 0 && blue == 0){
                    controller.addObject(new Coin(i*32,j*32,texturesManager.getCoin(),ObjectType.Coin));
                }else if (red == 0 && green == 255 && blue == 255){
                    controller.addObject(new Doors(i*32,j*32,texturesManager.getDoors(),ObjectType.Doors));
                }else if (red == 255 && green == 255 && blue == 0){
                    controller.addObject(new Chest(i*32,j*32,texturesManager.getChest(),ObjectType.Chest));
                }
            }
        }

    }
    /**
     * Loads the game's inventory from a saved file or creates a new one.
     */
    public void LoadInventory(){
        if(level_name==""){
            inv=new Inventory();
        }else{
            String name = level_name.substring(level_name.lastIndexOf("_") + 1, level_name.lastIndexOf("."));

            String final_path = "src/main/resources/saves/player_inventory_"+name+".json";
            System.out.println(final_path);

            try (FileInputStream fileIn = new FileInputStream(final_path);
                 ObjectInputStream in = new ObjectInputStream(fileIn)) {

                Object deserializedObject = in.readObject();

                inv = (Inventory) deserializedObject;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
    /**
     * Clock loop that has its own thread
     *
     */
    private void clockLoop() {
        while (clockRunning) {
            updateRealTime();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * Updates the clock
     *
     */
    private void updateRealTime() {
        Date currentTime = new Date();

        long elapsedTime = currentTime.getTime() - startTime;
        long elapsedMinutes = elapsedTime / (1000 * 60);
        if (elapsedMinutes >= 5) {
            running=false;
            System.exit(1);
        }
    }
    /**
     * Calculates time left
     *
     * @return the current time left in String format
     */
    private String getRemainingTime() {
        long elapsedTime = System.currentTimeMillis() - startTime;  //
        long remainingTime = 5 * 60 * 1000 - elapsedTime;  // time left

        if (remainingTime <= 0) {
            return "Time is out!";
        } else {
            long minutes = (remainingTime / 1000) / 60;
            long seconds = (remainingTime / 1000) % 60;
            return String.format("%02d:%02d", minutes, seconds);
        }
    }
    /**
     * Sets the game state to MENU.
     */
    public static void setStateMenu(){
        Game.State=STATE.GAME;
    }
    /**
     * Returns the current game state.
     *
     * @return the current game state
     */
    public static STATE getState() {
        return State;
    }
    /**
     * Pauses the game.
     */
    public void pauseGame(){
        isPaused=true;
    }
    /**
     * Unpauses the game.
     */
    public void unpauseGame(){
        isPaused=false;
    }
    /**
     * Checks if the game is paused.
     *
     * @return true if the game is paused, false otherwise
     */
    public boolean getIsPaused(){
        return isPaused;
    }
    /**
     * Checks if the game is over.
     *
     * @return true if the game is over, false otherwise
     */
    public static boolean isGameover() {
        return Gameover;
    }
    /**
     * Sets the game over state.
     *
     * @param gameover the game over state
     */
    public static void setGameover(boolean gameover) {
        Gameover = gameover;
    }
    /**
     * Checks if loggers are turned on.
     *
     * @return true if loggers are turned on, false otherwise
     */
    public boolean isLoggersOn() {
        return loggersOn;
    }
    /**
     * Sets the logger state.
     *
     * @param bool the logger state
     */
    public void setLogger(Boolean bool) {
        loggersOn=bool;
    }
    /**
     * Returns the game inventory.
     *
     * @return the game inventory
     */
    public Inventory getInv(){
        return inv;
    }

    /**
     * Sets the level name.
     *
     * @param level_name the level name
     */
    public void setLevel_name(String level_name) {
        this.level_name = level_name;
    }
    /**
     * Sets the game inventory.
     *
     * @param inventory the game inventory
     */
    public void setInventory(Inventory inventory){
        this.inv=inventory;
    }
}



