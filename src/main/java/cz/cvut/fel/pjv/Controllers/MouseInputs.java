package cz.cvut.fel.pjv.Controllers;

import cz.cvut.fel.pjv.Objects.ObjectType;
import cz.cvut.fel.pjv.Utils.Save;
import cz.cvut.fel.pjv.Utils.GameStates;
import cz.cvut.fel.pjv.Window.LevelChoice;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

/**
 * The MouseInputs class handles mouse input events for the game.
 */
public class MouseInputs extends MouseAdapter {
    private final Game game;
    private final GameStates gameStates;
    private final Controller controller;

    /**
     * Constructs a MouseInputs object.
     *
     * @param game          the game instance
     * @param gameStates    the gameStates instance
     * @param controller    the controller instance
     */
    public MouseInputs(Game game, GameStates gameStates, Controller controller) {
        this.game = game;
        this.gameStates = gameStates;
        this.controller = controller;
    }

    /**
     * Invoked when the mouse is clicked.
     *
     * @param e  the MouseEvent object that contains the event information
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        int mouseX = e.getX();
        int mouseY = e.getY();

        if (Game.getState() == Game.STATE.MENU) {
            if (mouseX >= 300 && mouseX <= 500 && mouseY >= 150 && mouseY <= 200) {
                Game.setStateMenu();
            } else if (mouseX >= 300 && mouseX <= 500 && mouseY >= 250 && mouseY <= 300) {
                SwingUtilities.invokeLater(() -> {
                    LevelChoice levelChoice = new LevelChoice(game);
                    levelChoice.setVisible(true);
                });
            } else if (mouseX >= 300 && mouseX <= 500 && mouseY >= 350 && mouseY <= 400) {
                System.exit(1);
            } else if (mouseX >= 300 && mouseX <= 700 && mouseY >= 450 && mouseY <= 500) {
                if (game.isLoggersOn() == true) {
                    System.out.println("Loggers off");
                    game.setLogger(false);
                } else if (game.isLoggersOn() == false) {
                    System.out.println("Loggers on");
                    game.setLogger(true);
                }
            }
        }
        if (Game.getState() == Game.STATE.GAME && game.getIsPaused()) {
            if (mouseX >= 300 && mouseX <= 500 && mouseY >= 150 && mouseY <= 200) {
                for (int i = 0; i < controller.object.size(); i++) {
                    controller.temObject = controller.object.get(i);
                    if (controller.temObject.getType() == ObjectType.Player) {
                        Save save = new Save(game, controller.temObject.getX(), controller.temObject.getY());
                        try {
                            save.SaveInventory(game.getInv()); //saving inventory
                            save.SaveGame();
                        } catch (IOException ex) {
                            throw new RuntimeException(ex);
                        }
                    }
                }
            } else if (mouseX >= 300 && mouseX <= 500 && mouseY >= 250 && mouseY <= 300) {
                System.exit(1);
            }
        }
        if (Game.isGameover()) {
            if (mouseX >= 300 && mouseX <= 500 && mouseY >= 150 && mouseY <= 200) {
                System.exit(1);
            }
        }
    }
}
