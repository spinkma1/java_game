package cz.cvut.fel.pjv.Images;

import java.io.IOException;

/**

 The TexturesManager class manages the loading and retrieval of various textures.
 */
public class TexturesManager {
    public Textures blocks;
    public Textures player;
    public Textures player_reverse;
    public Textures keys;
    public Textures spider;
    public Textures doors;
    public Textures coin;
    public Textures chest;

    /**

     Constructs a TexturesManager object and loads the textures from the specified paths.
     */
    public TexturesManager() {
        try {
            blocks = new Textures("/blocks.png");
            keys = new Textures("/Key.png");
            spider = new Textures("/Enemy1.png");
            player = new Textures("/1.png"); //player
            player_reverse = new Textures("/2.png"); //rever player sheet
            doors = new Textures("/Door.png");
            coin = new Textures("/Coin.png");
            chest = new Textures("/Chest.png");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Textures getBlocks() {
        return blocks;
    }

    public Textures getKeys() {
        return keys;
    }

    public Textures getPlayer_reverse() {
        return player_reverse;
    }

    public Textures getSpider() {
        return spider;
    }

    public Textures getPlayer() {
        return player;
    }

    public Textures getDoors() {
        return doors;
    }

    public Textures getCoin() {
        return coin;
    }

    public Textures getChest() {
        return chest;
    }
}







