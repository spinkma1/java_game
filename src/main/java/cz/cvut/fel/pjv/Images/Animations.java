package cz.cvut.fel.pjv.Images;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The Animations class represents an animated sequence of images.
 */
public class Animations {
    private int index = 0;
    private int count = 0;
    private final int speed;
    private final int frames;

    private final BufferedImage[] img;
    private BufferedImage curr_img;

    /**
     * Constructs an Animations object with the specified speed and images.
     *
     * @param speed the speed of the animation (number of frames per animation cycle)
     * @param args  the images to be used in the animation
     */
    public Animations(int speed, BufferedImage... args) { // SOURCE: https://www.youtube.com/watch?v=kzgNCEWUqUs&list=PLWms45O3n--54U-22GDqKMRGlXROOZtMx&index=13&ab_channel=RealTutsGML
        this.speed = speed;
        img = new BufferedImage[args.length];
        System.arraycopy(args, 0, img, 0, args.length);
        frames = args.length;
    }

    /**
     * Draws the current frame of the animation at the specified position.
     *
     * @param g the graphics context
     * @param x the x-coordinate of the position
     * @param y the y-coordinate of the position
     */
    public void drawAnimation(Graphics g, int x, int y) {
        g.drawImage(curr_img, x, y, null);
    }

    /**
     * Advances the animation to the next frame.
     */
    public void Animate() {
        index++;
        if (index > speed) {
            index = 0;
            nextFrame();
        }
    }

    /**
     * Sets the next frame of the animation as the current frame.
     */
    public void nextFrame() {
        for (int i = 0; i < frames; i++) {
            if (count == i) {
                curr_img = img[i];
            }
        }
        count++;
        if (count > frames) {
            count = 0;
        }
    }
}
