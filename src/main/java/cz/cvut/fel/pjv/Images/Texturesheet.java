package cz.cvut.fel.pjv.Images;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;

/**

 The Texturesheet class represents a sheet containing multiple subimages.
 */
public class Texturesheet implements Serializable {
    private final transient BufferedImage sheet;
    private final int subImageWidth;
    private final int subImageHeight;

    /**

     Constructs a Texturesheet object by reading the sheet image from the specified path.
     @param path the path to the sheet image file
     @param subImageWidth the width of each subimage
     @param subImageHeight the height of each subimage
     @throws IOException if an error occurs while reading the sheet image
     */
    public Texturesheet(String path, int subImageWidth, int subImageHeight) throws IOException {
        sheet = ImageIO.read(getClass().getResourceAsStream(path));
        this.subImageWidth = subImageWidth;
        this.subImageHeight = subImageHeight;
    }
    /**

     Returns a subimage from the texturesheet based on the specified coordinates.
     @param x the x-coordinate of the subimage
     @param y the y-coordinate of the subimage
     @return the subimage at the specified coordinates
     */
    public BufferedImage getSubimage(int x, int y) {
        return sheet.getSubimage(x * subImageWidth, y * subImageHeight, subImageWidth, subImageHeight);
    }
}