package cz.cvut.fel.pjv.Images;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;

/**
 * The ImagesManager class is responsible for loading images from files.
 */
public class ImagesManager implements Serializable {
    private BufferedImage img;

    /**
     * Loads an image from the specified file path.
     *
     * @param path the path to the image file
     * @return the loaded BufferedImage object
     * @throws IOException if an error occurs while reading the image file
     */
    public BufferedImage loadImage(String path) throws IOException {
        try {
            img = ImageIO.read(getClass().getResource(path));
        } catch (IOException e) {
            e.printStackTrace();
            e.toString();
            e.getClass();
        }
        return img;
    }
}

