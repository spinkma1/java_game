package cz.cvut.fel.pjv.Images;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;

/**
 * The Textures class represents a collection of subimages loaded from a texturesheet.
 */
public class Textures implements Serializable {
    transient Texturesheet sheet;
    private final transient BufferedImage[] subimages = new BufferedImage[20];

    /**
     * Constructs a Textures object by loading subimages from the specified texturesheet.
     *
     * @param path the path to the texturesheet file
     * @throws IOException if an error occurs while loading the texturesheet
     */
    public Textures(String path) throws IOException {
        sheet = new Texturesheet(path, 32, 32);
        subimages[0] = sheet.getSubimage(0, 0); // BLOCK1 or PLAYER1
        if (path.equals("/blocks.png") || path.equals("/Enemy1.png")) {
            subimages[1] = sheet.getSubimage(0, 1); // BLOCK2 or PLAYER5
        }
        if (path.equals("/1.png")) {
            subimages[1] = sheet.getSubimage(0, 1);
            subimages[2] = sheet.getSubimage(1, 0); // PLAYER2
            subimages[4] = sheet.getSubimage(2, 0); // PLAYER3
            subimages[12] = sheet.getSubimage(1, 1); // PLAYER6
            subimages[5] = sheet.getSubimage(2, 1); // PLAYER7
            subimages[6] = sheet.getSubimage(2, 2); // PLAYER8
            subimages[3] = sheet.getSubimage(2, 0); // PLAYER9
            subimages[7] = sheet.getSubimage(3, 2);
            // DEAD
            subimages[11] = sheet.getSubimage(0, 4);
            // ATTACK
            subimages[8] = sheet.getSubimage(0, 3);
            subimages[9] = sheet.getSubimage(1, 3);
            subimages[10] = sheet.getSubimage(2, 3);
            subimages[13] = sheet.getSubimage(3, 3);

        } else if (path.equals("/2.png")) {
            subimages[4] = sheet.getSubimage(3, 2); // PLAYER3
            subimages[12] = sheet.getSubimage(2, 1); // PLAYER6
            subimages[5] = sheet.getSubimage(1, 1); // PLAYER7
            subimages[6] = sheet.getSubimage(1, 2); // PLAYER8
            subimages[3] = sheet.getSubimage(1, 0); // PLAYER9
            // IDLE
            subimages[2] = sheet.getSubimage(2, 0); // PLAYER2
            subimages[1] = sheet.getSubimage(1, 0);
            subimages[7] = sheet.getSubimage(3, 1);

            // DEAD
            subimages[11] = sheet.getSubimage(3, 4);
            // ATTACK
            subimages[8] = sheet.getSubimage(3, 3);
            subimages[9] = sheet.getSubimage(2, 3);
            subimages[10] = sheet.getSubimage(1, 3);
            subimages[13] = sheet.getSubimage(0, 3);
        }
    }

    /**
     * Returns the first subimage.
     *
     * @return the first subimage
     */
    public BufferedImage getSubimage1() {
        return subimages[0];
    }

    /**
     * Returns the second subimage.
     *
     * @return the second subimage
     */
    public BufferedImage getSubimage2() {
        return subimages[1];
    }

    /**
     * Returns the third subimage.
     *
     * @return the third subimage
     */
    public BufferedImage getSubimage3() {
        return subimages[2];
    }

    /**
     * Returns the fourth subimage.
     *
     * @return the fourth subimage
     */
    public BufferedImage getSubimage4() {
        return subimages[3];
    }

    /**
     * Returns the fifth subimage.
     *
     * @return the fifth subimage
     */
    public BufferedImage getSubimage5() {
        return subimages[4];
    }

    /**
     * Returns the sixth subimage.
     *
     * @return the sixth subimage
     */
    public BufferedImage getSubimage6() {
        return subimages[5];
    }

    /**
     * Returns the seventh subimage.
     *
     * @return the seventh subimage
     */
    public BufferedImage getSubimage7() {
        return subimages[6];
    }

    /**
     * Returns the eighth subimage.
     *
     * @return the eighth subimage
     */
    public BufferedImage getSubimage8() {
        return subimages[7];
    }

    /**
     * Returns the ninth subimage.
     *
     * @return the ninth subimage
     */
    public BufferedImage getSubimage9() {
        return subimages[8];
    }

    /**
     * Returns the tenth subimage.
     *
     * @return the tenth subimage
     */
    public BufferedImage getSubimage10() {
        return subimages[9];
    }

    /**
     * Returns the eleventh subimage.
     *
     * @return the eleventh subimage
     */
    public BufferedImage getSubimage11() {
        return subimages[10];
    }

    /**
     * Returns the twelfth subimage.
     *
     * @return the twelfth subimage
     */
    public BufferedImage getSubimage12() {
        return subimages[11];
    }

    /**
     * Returns the thirteenth subimage.
     *
     * @return the thirteenth subimage
     */
    public BufferedImage getSubimage13() {
        return subimages[12];
    }

    /**
     * Returns the fourteenth subimage.
     *
     * @return the fourteenth subimage
     */
    public BufferedImage getSubimage14() {
        return subimages[13];
    }

    /**
     * Returns the fifteenth subimage.
     *
     * @return the fifteenth subimage
     */
    public BufferedImage getSubimage15() {
        return subimages[14];
    }

    /**
     * Returns the sixteenth subimage.
     *
     * @return the sixteenth subimage
     */
    public BufferedImage getSubimage16() {
        return subimages[15];
    }

    /**
     * Returns the seventeenth subimage.
     *
     * @return the seventeenth subimage
     */
    public BufferedImage getSubimage17() {
        return subimages[16];
    }

    /**
     * Returns the eighteenth subimage.
     *
     * @return the eighteenth subimage
     */
    public BufferedImage getSubimage18() {
        return subimages[17];
    }
}

