package cz.cvut.fel.pjv.Utils;

import cz.cvut.fel.pjv.Controllers.Controller;
import cz.cvut.fel.pjv.Images.Textures;
import cz.cvut.fel.pjv.Images.TexturesManager;
import cz.cvut.fel.pjv.Objects.Coin;
import cz.cvut.fel.pjv.Objects.Doors;
import cz.cvut.fel.pjv.Objects.Key;
import cz.cvut.fel.pjv.Objects.ObjectType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {
    private static TexturesManager manager;
    private static Textures tex;
    private static Textures texReverse;
    private static Controller controller;
    private static Inventory inv;

    @BeforeAll
    public static void setUp() throws IOException {
        manager = new TexturesManager();
        tex = new Textures("/1.png");
        texReverse = new Textures("/2.png");
        controller = new Controller();
        inv = new Inventory();
    }

    @Test
    void addItem() {
        Key key = new Key(32, 32, tex, ObjectType.Key);
        inv.addItem(key);
        Assertions.assertEquals(true,inv.getItems().contains(key));
    }

    @Test
    void removeItem() {
        inv = new Inventory();
        Key key = new Key(32, 32, tex, ObjectType.Key);
        inv.addItem(key);
        Assertions.assertEquals(true,inv.getItems().contains(key));
        inv.removeItem(key);
        Assertions.assertEquals(false,inv.getItems().contains(key));
    }

    @Test
    void returnSize() {
        inv = new Inventory();
        Key key = new Key(32, 32, tex, ObjectType.Key);
        Coin coin= new Coin(100, 100, tex, ObjectType.Coin);
        inv.addItem(key);
        inv.addItem(coin);
        Assertions.assertEquals(2,inv.getItems().size());
    }

    @Test
    void returnNumOfDoors() {
        inv = new Inventory();
        Key key = new Key(32, 32, tex, ObjectType.Key);
        Coin coin= new Coin(100, 100, tex, ObjectType.Coin);
        Doors doors = (new Doors(64,64,manager.getDoors(),ObjectType.Doors));
        inv.addItem(key);
        inv.addItem(coin);
        inv.addItem(doors);
        Assertions.assertEquals(1,inv.returnNumOfDoors());
    }

    @Test
    void returnNumOfCoins() {
        inv = new Inventory();
        Key key = new Key(32, 32, tex, ObjectType.Key);
        Coin coin= new Coin(100, 100, tex, ObjectType.Coin);
        Doors doors = (new Doors(64,64,manager.getDoors(),ObjectType.Doors));
        inv.addItem(key);
        inv.addItem(coin);
        inv.addItem(doors);
        Assertions.assertEquals(1,inv.returnNumOfCoins());
    }
}