package cz.cvut.fel.pjv.Objects;

import cz.cvut.fel.pjv.Controllers.Controller;
import cz.cvut.fel.pjv.Images.Textures;
import cz.cvut.fel.pjv.Images.TexturesManager;
import cz.cvut.fel.pjv.Utils.GameObject;
import cz.cvut.fel.pjv.Utils.Inventory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.awt.Rectangle;
import java.io.IOException;
import java.util.LinkedList;

public class PlayerTest {
    private static TexturesManager manager;
    private static Textures tex;
    private static Textures texReverse;
    private static Controller controller;
    private static Inventory inv;

    @BeforeAll
    public static void setUp() throws IOException {
        manager = new TexturesManager();
        tex = new Textures("/1.png");
        texReverse = new Textures("/2.png");
        controller = new Controller();
        inv = new Inventory();
    }

    @Test
    public void testPlayerBounds(){

        Player player = new Player(32, 32, tex, texReverse, ObjectType.Player, controller, inv);

        LinkedList<GameObject> objects = new LinkedList<>();
        objects.add(player);
        objects.add((new Block(64,64,0,manager.getBlocks(),ObjectType.Block)));
        player.tick(objects);


        Rectangle bounds = player.getBounds();
        //Assert hitboxů
        Assertions.assertEquals(7+32, bounds.x);
        Assertions.assertEquals(27+32, bounds.y);
        Assertions.assertEquals(17, bounds.width);
        Assertions.assertEquals(5, bounds.height);


        Rectangle boundsTop = player.getBoundsTop();

        Assertions.assertEquals(7+32, boundsTop.x);
        Assertions.assertEquals(32, boundsTop.y);
        Assertions.assertEquals(32-15, boundsTop.width);
        Assertions.assertEquals(5, boundsTop.height);

        Rectangle boundsLeft = player.getBoundsLeft();

        Assertions.assertEquals(32, boundsLeft.x);
        Assertions.assertEquals(37, boundsLeft.y);
        Assertions.assertEquals(5, boundsLeft.width);
        Assertions.assertEquals(22, boundsLeft.height);


        Rectangle boundsRight = player.getBoundsRight();

        Assertions.assertEquals(26+32, boundsRight.x);
        Assertions.assertEquals(5+32, boundsRight.y);
        Assertions.assertEquals(5, boundsRight.width);
        Assertions.assertEquals(22, boundsRight.height);


        System.out.println(player.getX()+"+"+ player.getY());

        player.setX(64);
        player.setY(64);

        player.tick(objects);

        System.out.println(player.getX()+"+"+ player.getY());

    }
    @Test
    public void testPlayerBlockCollision(){
        Player player = new Player(32, 32, tex, texReverse, ObjectType.Player, controller, inv);

        LinkedList<GameObject> objects = new LinkedList<>();
        objects.add(player);
        objects.add((new Block(64,64,0,manager.getBlocks(),ObjectType.Block)));
        player.tick(objects);

        System.out.println(player.getX()+"+"+ player.getY());
        Assertions.assertEquals(32,player.getX());
        Assertions.assertEquals(32,player.getY());
        player.setX(64);
        player.setY(64);

        player.tick(objects);

        Assertions.assertEquals(64,player.getX());
        Assertions.assertEquals(32,player.getY());

        System.out.println(player.getX()+"+"+ player.getY());

    }
    @Test
    public void testPlayerDoorCollision(){
        Player player = new Player(32, 32, tex, texReverse, ObjectType.Player, controller, inv);


        controller.addObject(player);
        Doors doors = (new Doors(64,64,manager.getDoors(),ObjectType.Doors));
        controller.addObject(doors);
        controller.tick();

        //System.out.println(player.getX()+"+"+ player.getY());
        Assertions.assertEquals(32,player.getX());
        Assertions.assertEquals(32,player.getY());
        player.setX(64);
        player.setY(32);

        controller.tick();

        Assertions.assertEquals(64,player.getX());
        Assertions.assertEquals(32.5,player.getY());

        System.out.println(player.getX()+"+"+ player.getY());

        player.setHasKey(true);

        player.setX(64);
        player.setY(64);

        controller.tick();

        //System.out.println(player.getX()+"+"+ player.getY());
        Assertions.assertEquals(false,controller.object.contains(doors));
    }
    @Test
    public void testPlayerSpiderCollision() {
        Player player = new Player(32, 32, tex, texReverse, ObjectType.Player, controller, inv);

        controller.addObject(player);
        Spider spider = (new Spider(64,64,manager.getSpider(),ObjectType.Spider,controller));
        controller.addObject(spider);
        controller.tick();
        //System.out.println(player.getX()+"+"+ player.getY());
        Assertions.assertEquals(32,player.getX());
        Assertions.assertEquals(32,player.getY());
        player.isAttacking=false;
        player.setX(50);
        player.setY(64);
        controller.tick();
        //System.out.println(player.getX()+"+"+ player.getY());
        Assertions.assertEquals(false,controller.object.contains(player));


    }
    @Test
    public void testPlayerSpiderDeadCollision() {
        Player player = new Player(32, 32, tex, texReverse, ObjectType.Player, controller, inv);

        controller.addObject(player);
        Spider spider = (new Spider(64,64,manager.getSpider(),ObjectType.Spider,controller));
        controller.addObject(spider);
        controller.tick();
        //System.out.println(player.getX()+"+"+ player.getY());
        Assertions.assertEquals(32,player.getX());
        Assertions.assertEquals(32,player.getY());
        player.isAttacking=true;
        player.setX(50);
        player.setY(64);
        controller.tick();
        //System.out.println(player.getX()+"+"+ player.getY());
        Assertions.assertEquals(true,controller.object.contains(player));
        Assertions.assertEquals(false,controller.object.contains(spider));


    }
}
